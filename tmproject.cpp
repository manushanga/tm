 
#include "tmproject.h"

#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <iostream>
using namespace std;
TmProject::TmProject(QString filename)
{
    mFilename=filename;
    projectPath = filename.section('/',0,-2);
    QFile f(filename);
    if (f.exists()) {
        f.open(QFile::ReadOnly | QFile::Text);
        doc.setContent(f.readAll());
        std::cout<<doc.toString().toStdString() <<std::endl;

        root=doc.documentElement();
        auto srcele = root.firstChild().toElement();
        auto tgtele = srcele.nextSiblingElement();
        src=srcele.firstChild().toText();
        if (tgtele.firstChild().isNull()) {
            tgt=doc.createTextNode("");
            tgtele.appendChild(tgt);

        } else {
            tgt=tgtele.firstChild().toText();
        }
        f.close();
    } else {
        root=doc.createElement("tmProject");
        doc.appendChild(root);
        root.setAttribute("dictionary","");
        root.setAttribute("srcCorpus","");
        root.setAttribute("tgtCorpus","");
        root.setAttribute("srcLang","");
        root.setAttribute("tgtLang","");

        src=doc.createTextNode("");
        tgt=doc.createTextNode("");

        auto srcele=doc.createElement("source");
        auto tgtele=doc.createElement("target");

        root.appendChild(srcele);
        root.appendChild(tgtele);
        srcele.appendChild(src);
        tgtele.appendChild(tgt);
    }

}

TmProject::TmProject(QString source, QString target,
                     QString srcLang,
                     QString tgtLang,
                     QString dictionary,
                     QString srcCorpus,
                     QString tgtCorpus)
{
    root=doc.createElement("tmProject");
    doc.appendChild(root);
    root.setAttribute("dictionary",getRelative(dictionary));
    root.setAttribute("srcCorpus",getRelative(srcCorpus));
    root.setAttribute("tgtCorpus",getRelative(tgtCorpus));
    root.setAttribute("srcLang",srcLang);
    root.setAttribute("tgtLang",tgtLang);

    src=doc.createTextNode("");
    src.setData(source);
    tgt=doc.createTextNode("");
    tgt.setData(target);

    auto srcele=doc.createElement("source");
    auto tgtele=doc.createElement("target");

    root.appendChild(srcele);
    root.appendChild(tgtele);
    srcele.appendChild(src);
    tgtele.appendChild(tgt);

}

void TmProject::setFilename(QString name)
{
    mFilename = name;
}

QString TmProject::getProperty(QString name)
{
    return root.attribute(name);
}

void TmProject::setProperty(QString name, QString value)
{
    if (name == "dictionary" ||
            name == "srcCorpus" ||
            name == "tgtCorpus" ) {
        root.setAttribute(name, getRelative(value));
    } else {
        root.setAttribute(name,value);
    }
}

void TmProject::setSource(QString& text)
{
    src.setData(text);
}

QString TmProject::getRelative(QString filename)
{
    QString path = mFilename.section('/',0,-2);

    if (filename.contains(path)) {
        std::cout<<filename.replace(path,".").toStdString()<<std::endl;
        return filename.replace(path,".");
    } else {
        return filename;
    }

}

QString TmProject::getSource()
{
    return src.data();
}

QString TmProject::getTarget()
{
    return tgt.data();
}

QString TmProject::getInterleaved()
{
    QStringList srcl = src.data().split('\n');
    QStringList tgtl = tgt.data().split('\n');
    QString text;

    text.reserve(20000);
    for (int i=0;i<srcl.size();i++) {
        if (tgtl[i].trimmed().size() == 0) {
            text += srcl[i] + "\n";
        } else {
            text += tgtl[i] + "\n";
        }
    }
    return text;
}
void TmProject::setTarget(QString &text)
{

    tgt.setData(text);
     cout<<doc.toString().toStdString()<<endl;
}

void TmProject::save()
{
    bool fx=false;
    QFile f(mFilename);
    fx=f.open(QFile::WriteOnly | QIODevice::Text );
    QTextStream ts(&f);

    //ts.setCodec("utf-8");

    ts<<doc.toString();
    f.close();
}

TmProject::~TmProject()
{
}
