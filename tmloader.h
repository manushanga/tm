#ifndef TMLOADER_H
#define TMLOADER_H
#include <QDialog>
#include <QLabel>
#include <QVBoxLayout>
class TmLoader : public QDialog {
    Q_OBJECT
private:
    QLabel caption;
    QVBoxLayout lay;
public:
    TmLoader(QString text, QWidget *parent=0): QDialog(parent) {
        setWindowFlags(Qt::FramelessWindowHint);
        QFont f;
        f.setPixelSize(12);
        setModal(true);
        lay.addWidget(&caption);
        caption.setFont(f);
        caption.setText(text);
        setLayout(&lay);
        show();
    }
};
#endif // TMLOADER_H
