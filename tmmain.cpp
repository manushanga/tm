#include "tmmain.h"
#include "ui_tmmain.h"

#include <QInputDialog>
#include <QMessageBox>
#include <QTextCodec>
#include <QLabel>
#include <iostream>
#include <QStandardItemModel>
int getComboBoxIdx(QComboBox *box, QString text){
    for(int i=0;i<box->model()->rowCount();i++) {
        if (text == box->itemText(i)) {
            return i;
        }
    }
}
tmMain::tmMain(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::tmMain),
    prj(NULL),
    initDone(false)
{
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("utf-8"));

    TmTmx s;

    ui->setupUi(this);
    mBox = new QVBoxLayout();
    TmSettings::getSingleton()->getValue("version","0.1b");
    ui->scrollArea->widget()->setLayout(mBox);
    ui->cbSrcLang->addItems(TmLanguage::getSingleton()->getLanguages());
    ui->cbTgtLang->addItems(TmLanguage::getSingleton()->getLanguages());

    installEventFilter(this);
    initDone=false;
}

void tmMain::load()
{
    foreach (auto a, mRows) {
        mBox->addWidget(a);
        qApp->processEvents();
    }
}

void tmMain::clear()
{
    foreach (auto a, mRows) {
        mBox->removeWidget(a);
        delete a;
    }
    mRows.clear();
    qApp->processEvents();
}

bool tmMain::eventFilter(QObject *o, QEvent *e)
{
    if (e->type() == QEvent::Close) {
        QApplication::exit(0);
    }
    if (e->type() == QEvent::Resize) {
        auto g=ui->lbxx->geometry();

        ui->lbxx->setGeometry(width()/2 - ui->lbxx->width()/2,
                              height()/2.5 - ui->lbxx->height()/2,
                              ui->lbxx->width(),
                              ui->lbxx->height());
    }
    return QMainWindow::eventFilter(o,e);
}
tmMain::~tmMain()
{

    TmMatcherManager::getSingleton()->loadFromMatcher();
    if (prj)
        delete prj;
    delete ui;

}

void tmMain::on_actionExit_triggered()
{
    QApplication::exit();
}

void tmMain::on_actionNew_triggered()
{
    ui->lbxx->setVisible(false);
    if (prj) {
        delete prj;
        prj=NULL;
        ui->cbSrcLang->setEnabled(false);
        ui->cbTgtLang->setEnabled(false);
    }

    clear();

    auto fprj=QFileDialog::getSaveFileName(this,"New Project",".","TM Projects (*.xml)");
    if (fprj.size()>0) {
        QFile f(fprj);
        if (f.exists())
            f.remove();
        prj = new TmProject(fprj);
    } else {
        return;
    }

    QString name=QFileDialog::getOpenFileName(this,"Open Source Text",".","Text files(*.txt)");
    QString allText;
    allText.reserve(200000);
    if (name.size()>0) {
        QFile a(name);
        a.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&a);
        ts.setCodec("utf-8");
        while (!ts.atEnd()) {
            auto t = ts.readLine();
            mRows.append(new TmRow(t));
            allText.append(t+"\n");
        }
        a.close();
    }

    prj->setSource(allText);
    load();
    QString src=QInputDialog::getItem(this,"TM","Select source language",TmLanguage::getSingleton()->getLanguages(),0,false);
    QString tgt=QInputDialog::getItem(this,"TM","Select target language",TmLanguage::getSingleton()->getLanguages(),0,false);

    TmMatcherManager::getSingleton()->setLangs(src,tgt);
    prj->setProperty("srcLang",src);
    prj->setProperty("tgtLang",tgt);
    ui->cbSrcLang->setCurrentIndex(getComboBoxIdx(ui->cbSrcLang,src));
    ui->cbTgtLang->setCurrentIndex(getComboBoxIdx(ui->cbTgtLang,tgt));

    ui->cbSrcLang->setEnabled(true);
    ui->cbTgtLang->setEnabled(true);


/*
    QStringList choices;
    bool ok;
    choices<<"Translation Memory"<<"Parallel Corpus"<<"Both";
    QString choice=QInputDialog::getItem(this,"Translation sources","Select your sources",choices,0,false,&ok);
    if (ok) {
        if (choice == "Translation Memory" || choice == "Both"){

        }
        if (choice == "Parallel Corpus" || choice == "Both") {
            auto csrc=QFileDialog::getOpenFileName(this,"TM Source Corpus",".","TM Parallel Corpus (*.txt)");
            auto ctgt=QFileDialog::getOpenFileName(this,"TM Target Corpus",".","TM Parallel Corpus (*.txt)");
            if (csrc.size()>0 && ctgt.size()>0){
                TmMatcher<QString, QString>::getSingleton()->loadFromFile(csrc,ctgt);
                prj->setProperty("srcCorpus",csrc);
                prj->setProperty("tgtCorpus",ctgt);

            }
        }
    }*/
    QMessageBox msg(QMessageBox::Question,"Dictionary","Do you have a dictionary?",QMessageBox::Yes | QMessageBox::No);
    msg.exec();
    if (msg.result() == QMessageBox::Yes ) {

        auto dict=QFileDialog::getOpenFileName(this,"Dictionary",".","TM Dictionaries (*.txt)");
        if (dict.size()>0){
            TmDictionary::getSingleton()->loadFromFile(dict);
            prj->setProperty("dictionary",dict);
        }
    }
}

void tmMain::on_actionSave_triggered()
{
    if (!prj){
        QMessageBox(QMessageBox::Information,"TM","A project is not loaded, load one and try again.").exec();
        ui->cbSrcLang->setEnabled(false);
        ui->cbTgtLang->setEnabled(false);
        return;
    }

    if (prj){
        QString allText;
        allText.reserve(200000);
        foreach (auto a, mRows) {
            allText.append( a->getText()+"\n");
        }
        prj->setTarget(allText);
        prj->save();

        QMessageBox(QMessageBox::Information,"tm","Project saved.").exec();
    }
}

void tmMain::on_actionOpen_triggered()
{
    ui->lbxx->setVisible(false);
    if (prj) {
        delete prj;
        prj=NULL;
        ui->cbSrcLang->setEnabled(false);
        ui->cbTgtLang->setEnabled(false);
    }
    auto fprj=QFileDialog::getOpenFileName(this,"Open Project",".","TM Projects (*.xml)");
    if (fprj.size()>0) {
        prj = new TmProject(fprj);

        QString ax=prj->getSource();
        QString bx=prj->getTarget();

        QTextStream ts(&ax);
        QTextStream tst(&bx);

        clear();
        while (!ts.atEnd()) {
            mRows.append(new TmRow(ts.readLine(),tst.readLine()));
        }
        qApp->processEvents();
        TmLoader lrr("Loading File");
        load();
        lrr.hide();
        if (prj->getProperty("dictionary").size() >0)
            TmDictionary::getSingleton()->loadFromFile(prj->getProperty("dictionary"));

        TmMatcherManager::getSingleton()->setLangs(prj->getProperty("srcLang"),
                                                   prj->getProperty("tgtLang"));

        TmLoader lxx("Loading Corpuses");
        std::cout<< QDir::cleanPath(QDir(prj->getProjectPath()).filePath(prj->getProperty("tgtCorpus"))).toStdString() <<std::endl;
        if (prj->getProperty("srcCorpus").size() > 0 && prj->getProperty("tgtCorpus").size() > 0)
            TmMatcher::getSingleton()->loadFromFile(QDir::cleanPath(QDir(prj->getProjectPath()).filePath(prj->getProperty("srcCorpus"))),
                                                    QDir::cleanPath(QDir(prj->getProjectPath()).filePath(prj->getProperty("tgtCorpus"))) );

        lxx.hide();

        TmLoader lod("Loading TM db ...");

        lod.hide();



        ui->cbSrcLang->setCurrentIndex(getComboBoxIdx(ui->cbSrcLang,prj->getProperty("srcLang")));
        ui->cbTgtLang->setCurrentIndex(getComboBoxIdx(ui->cbTgtLang,prj->getProperty("tgtLang")));
        ui->cbSrcLang->setEnabled(true);
        ui->cbTgtLang->setEnabled(true);
        initDone = true;

    }
}



void tmMain::on_actionSet_corpus_files_triggered()
{
    if (!prj){
        QMessageBox(QMessageBox::Information,"TM","A project is not loaded, load one and try again.").exec();
        return;
    }

    auto csrc=QFileDialog::getOpenFileName(this,"TM Source Corpus",".","TM Parallel Corpus (*.txt)");
    auto ctgt=QFileDialog::getOpenFileName(this,"TM Target Corpus",".","TM Parallel Corpus (*.txt)");
    if (csrc.size()>0 && ctgt.size()>0){
        TmMatcher::getSingleton()->loadFromFile(csrc,ctgt);
        prj->setProperty("srcCorpus",csrc);
        prj->setProperty("tgtCorpus",ctgt);

    }
}

void tmMain::on_actionSet_Dictionary_triggered()
{
    if (!prj){
        QMessageBox(QMessageBox::Information,"TM","A project is not loaded, load one and try again.").exec();
        return;
    }

    auto dict=QFileDialog::getOpenFileName(this,"Dictionary",".","TM Dictionaries (*.txt)");
    if (dict.size()>0 ) {

        prj->setProperty("dictionary",dict);
        TmDictionary::getSingleton()->loadFromFile(dict);
    }
}

void tmMain::on_actionLoad_source_text_triggered()
{
    if (!prj){
        QMessageBox(QMessageBox::Information,"TM","A project is not loaded, load one and try again.").exec();
        return;
    }

    QString name=QFileDialog::getOpenFileName(this,"Open Source Text",".","Text files(*.txt)");
    QString allText;
    allText.reserve(200000);

    clear();
    if (name.size()>0) {
        QFile a(name);
        a.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&a);
        ts.setCodec("utf-8");
        while (!ts.atEnd()) {
            auto t = ts.readLine();
            mRows.append(new TmRow(t));
            allText.append(t+"\n");
        }
        a.close();
    }
    prj->setSource(allText);
    load();
}


void tmMain::on_actionAbout_triggered()
{
    QDialog abt;
    QLabel lbl;
    QVBoxLayout l;
    abt.setLayout(&l);
    l.addWidget(&lbl);
    lbl.setText( "<center><b><div style=\"font-size:12pt\">TranslationMemory</div></b> <br/><br/> By <br/> Madura A. Shelton <br/> for <br/> UCSC LTRL</center><br/> (C) 2013 LTRL, UCSC ");
    abt.setFixedWidth(300);
    abt.exec();

}

void tmMain::on_actionLoad_Translation_Memory_triggered()
{
    if (!prj){
        QMessageBox(QMessageBox::Information,"TM","A project is not loaded, load one and try again.").exec();
        return;
    }

    QString name=QFileDialog::getOpenFileName(this,"Open Translation Memory Text",".","TMX files(*.tmx)");
    if (!name.isEmpty()){
        TmMatcherManager::getSingleton()->loadFromFile(name);
    }
}

void tmMain::on_actionSave_Translation_Memory_triggered()
{
    auto name=QFileDialog::getSaveFileName(this,"Select Destination",".","TMX File (*.tmx)");
    TmMatcherManager::getSingleton()->loadFromMatcher();
    TmMatcherManager::getSingleton()->saveToFile(name);
}

void tmMain::on_pbSave_clicked()
{
    if (!prj){
        QMessageBox(QMessageBox::Information,"TM","A project is not loaded, load one and try again.").exec();
        return;
    }

    auto name = QFileDialog::getSaveFileName(this,"Filename to save translation","","Text File (*.txt)");
    if (name.size()) {

        QString srcL,tgtL;
        srcL.reserve(200000);
        tgtL.reserve(200000);

        for (int i=0;i<mRows.size();i++ ) {
            srcL += mRows[i]->getSource() +"\n";
            tgtL += mRows[i]->getText() +"\n";
        }
        prj->setSource(srcL);
        prj->setTarget(tgtL);

        QFile f(name);
        f.open(QFile::WriteOnly | QFile::Text);
        QTextStream ts(&f);
        ts << prj->getInterleaved();
        f.close();
    }
}

void tmMain::on_cbSrcLang_currentIndexChanged(const QString &arg1)
{
    if (!initDone)
        return;
    ui->lbDisplay->setText("Changing Translation Memory...");
    qApp->processEvents();
    TmLoader lod("Loading TM db ...");
    TmMatcherManager::getSingleton()->setLangs(arg1, ui->cbTgtLang->currentText());
    lod.hide();
    ui->lbDisplay->setText("Done.");
}

void tmMain::on_cbTgtLang_currentIndexChanged(const QString &arg1)
{
    if (!initDone)
        return;
    ui->lbDisplay->setText("Changing Translation Memory...");
    qApp->processEvents();
    TmLoader lod("Loading TM db ...");
    TmMatcherManager::getSingleton()->setLangs(ui->cbSrcLang->currentText(), ui->cbTgtLang->currentText());
    lod.hide();
    ui->lbDisplay->setText("Done.");
}
