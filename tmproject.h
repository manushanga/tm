#include <QDomDocument>
#include <QString>

#ifndef TMPROJECT_H
#define TMPROJECT_H

class TmProject
{
public:
    TmProject(QString filename);
    TmProject(QString source, QString target, QString srcLang, QString tgtLang,
              QString dictionary="",
              QString srcCorpus="",
              QString tgtCorpus="");
    void setFilename(QString name);
    QString getProperty(QString name);
    void setProperty(QString name, QString value);
    void setSource(QString &text);
    QString getRelative(QString filename);
    QString getSource();
    QString getTarget();
    void setTarget(QString &text);
    void save();
    QString getInterleaved();
    QString getProjectPath(){ return projectPath; }
    ~TmProject();
private:
    QDomElement root;
    QDomText src,tgt;
    QDomDocument doc;
    QString mFilename;
    QString projectPath;
};

#endif // TMPROJECT_H
