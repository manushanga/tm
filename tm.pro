#-------------------------------------------------
#
# Project created by QtCreator 2013-10-04T20:58:14
#
#-------------------------------------------------

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tm
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++11
SOURCES += main.cpp\
        tmmain.cpp \
    tmproject.cpp \
    tmlabel.cpp \
    tmrow.cpp \
    tmcompletor.cpp \
    tmmatcher.cpp \
    tmedit.cpp \
    tmdictionary.cpp \
    tmtmx.cpp

HEADERS  += tmmain.h \
    tmproject.h \
    tmlabel.h \
    tmrow.h \
    tmcompletor.h \
    tmcompletormanager.h \
    tmmatcher.h \
    tmedit.h \
    tmdictionary.h \
    tmtmx.h \
    tmlanguage.h \
    tmmatchermanager.h \
    tmsettings.h \
    tmnode.h \
    tmloader.h

FORMS    += tmmain.ui \
    tmcompletor.ui
 
