#ifndef TMCOMPLETOR_H
#define TMCOMPLETOR_H

#include "tmrow.h"

#include <QDialog>
#include <QStandardItemModel>

namespace Ui {
class TmCompletor;
}

class TmCompletor : public QDialog
{
    Q_OBJECT

public:
    explicit TmCompletor(QWidget *parent = 0);
    void setPosition(int x, int y);
    void populateList(QList<QString> &list);
    void join(TmRow *row);
    bool eventFilter(QObject *o, QEvent *e);
    ~TmCompletor();

private slots:
    void on_listView_clicked(const QModelIndex &index);

private:
    Ui::TmCompletor *ui;
    TmRow *mRow;
    QStandardItemModel *m;
};

#endif // TMCOMPLETOR_H
