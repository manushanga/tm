 
#include "tmlabel.h"

#include <iostream>

TmLabel::TmLabel(QString text, QWidget *parent) :
    QWidget(parent)
{
    installEventFilter(this);
    mTop=false;
    mText=text;
#ifdef WIN32
    QWidget::setFont(QFont("Iskoola Pota",11,2));
#endif
    setFixedHeight(fontMetrics().height()*1.2+10);
    setFixedWidth(fontMetrics().width(text)*1.2+10);
}

void TmLabel::paintEvent(QPaintEvent *)
{
    QPainter p(this);

    p.drawText(width()/2 - fontMetrics().width(mText)/2 ,4+fontMetrics().height(),mText);

    if (mTop) {
        QPen x;
        x.setWidth(2);
        x.setColor(QColor("red"));
        p.setPen(x);
        p.drawRoundedRect(4,4,width()-8,height()-8,4,4);
    }


}

void TmLabel::mousePressEvent(QMouseEvent *me)
{
    if (me->button() == Qt::LeftButton ) {

        emit clicked(mText);
    }
}
