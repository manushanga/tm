#include "tmedit.h"

#include <QMouseEvent>

TmEdit::TmEdit(QWidget *parent) :
    QLineEdit(parent)
{
}

void TmEdit::mousePressEvent(QMouseEvent *)
{
    emit clicked();
}
