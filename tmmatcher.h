#include <QString>
#include <QList>
#include <QSet>
#include <QHash>
#include <vector>
#include <QStringList>
#include <iostream>
#include <QDebug>

#ifndef TMMATCHER_H
#define TMMATCHER_H

#define MAXHASH 9999999
#define MAXEDITDIST 3

class TmMatcher
{
public:
    void retrieve_hash_rec(QStringList& words, int level, int word_level, int hash_so_far, QSet<int>& hash_list) {
        if (word_level == words.size()) {
            hash_list.insert(hash_so_far);

        } else {
            int d=0;
            while (d < level) {
                auto it=mWords.find(words[word_level]);
                if (it != mWords.end()) {
                    int hash_so_far_tmp=hash_so_far;
                    hash_so_far_tmp += (it.value())*1046527;
                    hash_so_far_tmp ^= 17551;
                    hash_so_far_tmp %= MAXHASH;
                    retrieve_hash_rec(words,level,word_level+1,hash_so_far_tmp,hash_list);
                }
                words[word_level].chop(1);
                d++;
            }
        }
    }
    int submit_hash(QStringList& words) {
        int hash_so_far=0;
        foreach(QString a,words){

            hash_so_far += mWords[a]*1046527;
            hash_so_far ^= 17551;
            hash_so_far %= MAXHASH;

        }
        return hash_so_far;

    }
    void addWord(QString word){
        QString wordx = word;
        int d=0;
        while (d<MAXEDITDIST) {
            auto it=mWords.find(wordx);
            if (wordx.length()>0 && it == mWords.end()) {
                mWords.insert(wordx,mCurrentWordId);
            }
            wordx.chop(1);
            d++;
        }

        mCurrentWordId++;

    }
    static int levenshtein_distance(const QString& s1, const QString& s2) {
        int len1 = s1.size(), len2 = s2.size();
        std::vector<int> col(len2+1), prevCol(len2+1);

        for (size_t i = 0; i < prevCol.size(); i++)
                prevCol[i] = i;
        for (size_t i = 0; i < len1; i++) {
            col[0] = i+1;
            for (int j = 0; j < len2; j++)
                col[j+1] = std::min( std::min( 1 + col[j], 1 + prevCol[1 + j]),
                                    prevCol[j] + (s1.at(i)==s2.at(j) ? 0 : 1) );
            col.swap(prevCol);
        }
        return prevCol[len2];
    }

    TmMatcher() : mCurrentWordId(0)
    {
    }
    static QString cleanString(QString text) {
        QString ret;
        for (int i=0;i<text.length();i++) {
            QChar xx=text[i];
            if (xx.isLetter() || xx.isSpace())
                ret.append(xx);
        }
        return ret;
    }

    static TmMatcher *getSingleton()
    {
        static TmMatcher tmm;
        return &tmm;
    }

    void addRow(QString src, QString tgt)
    {
        qDebug()<<"adding"<<src<<tgt;
        src = src.trimmed();
        src = cleanString(src);
        QStringList ls=src.split(' ',QString::SkipEmptyParts);

        foreach(QString a, ls){
            addWord(a);
        }
        int sentence_id = submit_hash(ls);
        std::cout<<sentence_id<<std::endl;
        auto it=mList.find(sentence_id);
        if (it == mList.end()) {
            mList.insert(sentence_id, TmData(src, tgt));
        } else {
            it.value().src_tgt.insert(src,tgt);

        }
    }

    int match(QString src, int level, QList<QString> &list)
    {
        src = cleanString(src);
        qDebug()<<src;
        QStringList ls=src.split(' ',QString::SkipEmptyParts);
        qDebug()<<"size"<<ls.size();
        QSet<int> hash_list;
        retrieve_hash_rec(ls,MAXEDITDIST,0,0,hash_list);

        foreach (int a, hash_list){
            foreach (QString x,mList[a].src_tgt.values()){
                list.append(x);
            }
        }
        return 0;
    }
    QHash<QString, QString> getData(){

        QHash<QString, QString> x;
        for (QHash<int, TmData>::iterator a=mList.begin();a!=mList.end();a++) {

            foreach (QString b, a.value().src_tgt.keys() ){
                x.insert(b,a.value().src_tgt[b]);
            }
        }
        return x;
    }
    QList<QString> getAll()
    {
        QList<QString> all;
        for (auto a=mList.begin();a!=mList.end();a++) {
           // all.append(a->key());
        }
        return all;
    }
    void clear() {
        mCurrentWordId = 0;
        mList.clear();
    }
    void loadFromFile(QString src, QString tgt);
private:
    struct TmData{
        QHash<QString, QString> src_tgt;
        TmData(){}
        TmData(QString _src, QString _tgt)  { src_tgt.insert(_src,_tgt); }
    };
    // string hash for words, each word gets an id
    QHash<QString, int> mWords;

    // sentence hash from the word ids
    QHash<int, TmData> mList;
    int mCurrentWordId;
};

#endif // TMMATCHER_H
