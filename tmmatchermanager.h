#ifndef TMMATCHERMANAGER_H
#define TMMATCHERMANAGER_H
#include "tmnode.h"
#include "tmtmx.h"
#include "tmmatcher.h"
#include "tmsettings.h"
#include "tmloader.h"

#include <QMessageBox>
#include <QSettings>

class TmMatcherManager{
private:
    QVector< QVector <TmNode> > data;
    QString srcLang, tgtLang;
    QHash<QString, int> reverseData;
public:
    TmMatcherManager(): srcLang(""), tgtLang("") {


    }
    void setLangs(QString src, QString tgt){
        srcLang =src;
        tgtLang=tgt;

        QString pp =TmSettings::getSingleton()->getSettingsPath() + "/memory.tmx";
        data.clear();

        if (QFile(pp).exists()) {
            loadFromFile(pp);
            loadMatcher();
        }

    }
    void loadMatcher(){
        int i=0;
        TmMatcher::getSingleton()->clear();

        foreach (auto row, data) {
            QString lines[2];
            foreach (TmNode trans, row) {
                if (trans.lang == srcLang)
                {
                    lines[0]=trans.text;
                } else if (trans.lang == tgtLang) {
                    lines[1]=trans.text;
                }
                reverseData.insert(trans.text,i);
            }
            if (!lines[0].isEmpty() && !lines[1].isEmpty() ) {
                TmMatcher::getSingleton()->addRow(lines[0],lines[1]);
            }
            i++;
        }
    }
    void loadFromMatcher(){
        QHash<QString,QString> hash=TmMatcher::getSingleton()->getData();
        for (auto it=hash.begin();it!=hash.end();it++) {
            auto itrdsrc =reverseData.find(it.key());
            auto itrdtgt = reverseData.find(it.value());
            qDebug()<<"mlo"<<it.key();
            if (itrdsrc == reverseData.end() && itrdtgt == reverseData.end()) {
                TmNode tn1;
                tn1.lang = srcLang;
                tn1.text = it.key();

                TmNode tn2;
                tn2.lang = tgtLang;
                tn2.text = it.value();

                QVector<TmNode> vt;
                vt.push_back(tn1);
                vt.push_back(tn2);

                data.push_back(vt);
            } else if (itrdsrc == reverseData.end()) {
                TmNode tn1;
                tn1.lang = srcLang;
                tn1.text = it.key();

                data[reverseData[it.value()]].push_back(tn1);
            } else if (itrdtgt == reverseData.end()) {
                TmNode tn2;
                tn2.lang = tgtLang;
                tn2.text = it.value();
                data[reverseData[it.key()]].push_back(tn2);
            }
        }

    }
    inline int match(QString src, int level, QList<QString> &list)
    {
        return TmMatcher::getSingleton()->match(src,level,list);
    }
    void addRow(QString src, QString tgt)
    {
        TmMatcher::getSingleton()->addRow(src,tgt);
    }
    void loadFromFile(QString file) {
        if (file.endsWith("tmx")) {
            TmTmx::loadFromFile(data, file);
        } else {
            QMessageBox(QMessageBox::Warning,"tm","Unsupported file").exec();
        }
    }
    void saveToFile(QString file) {
        if (file.endsWith("tmx")) {
            TmTmx::saveToFile(data,file);
        } else {
            QMessageBox(QMessageBox::Warning,"tm","Unsupported file").exec();
        }
    }
    void clear(){
        data.clear();
        reverseData.clear();
    }
    ~TmMatcherManager(){
        QString pp =TmSettings::getSingleton()->getSettingsPath() + "/memory.tmx";
        cout<<pp.toStdString()<<endl;
        saveToFile(pp);
    }
    static TmMatcherManager *getSingleton(){
        static TmMatcherManager mm;
        return &mm;
    }
};
#endif // TMMATCHERMANAGER_H
