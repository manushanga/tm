#include "tmcompletor.h"
#include "ui_tmcompletor.h"

#include <QStandardItem>

TmCompletor::TmCompletor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TmCompletor),
    mRow(NULL)
{
    ui->setupUi(this);
    setWindowFlags(Qt::ToolTip | Qt::FramelessWindowHint );
    setMaximumHeight(200);
    setMaximumWidth(600);
    setModal(false);
    m=new QStandardItemModel;
    QFont f;
    f.setPointSize(11);

    ui->listView->setFont(f);
    ui->listView->setModel(m);
    installEventFilter(this);
}

void TmCompletor::setPosition(int x, int y)
{
    QRect r = geometry();
    r.setX(x);
    r.setY(y);
    setGeometry(r);
}

TmCompletor::~TmCompletor()
{
    delete ui;
}

void TmCompletor::populateList(QList<QString> &list)
{
    m->clear();
    int i=0;
    QFont f("Iskoola Pota");
    foreach (QString a, list) {
        auto xx =new QStandardItem(a);
        xx->setFont(f);
        m->setItem(i,xx);
        i++;
    }
    
}

void TmCompletor::join(TmRow *row)
{
    mRow=row;
}

bool TmCompletor::eventFilter(QObject *o, QEvent *e)
{
    if (e->type()==QEvent::Leave) {
        hide();
    }
    return QWidget::eventFilter(o,e);
}

void TmCompletor::on_listView_clicked(const QModelIndex &index)
{
    if (mRow){
        mRow->setText(index.data().toString());
    }
    hide();
}
