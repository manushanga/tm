#include "tmmatcher.h"

#include <QFile>
#include <QTextStream>
#include <QDebug>
void TmMatcher::loadFromFile(QString src, QString tgt)
{

    qDebug()<<src<<tgt;
    if (src.endsWith("txt") && tgt.endsWith("txt")) {
        QFile f(src),g(tgt);

        f.open(QFile::ReadOnly| QFile::Text);
        g.open(QFile::ReadOnly| QFile::Text);

        QTextStream tsf(&f),tsg(&g);

        while (!tsf.atEnd() && !tsg.atEnd()) {
            addRow(tsf.readLine(),tsg.readLine());
        }
        f.close();
        g.close();
    }
}
