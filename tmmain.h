#ifndef TMMAIN_H
#define TMMAIN_H

#include "tmproject.h"
#include "tmlabel.h"
#include "tmrow.h"
#include "tmmatcher.h"
#include "tmdictionary.h"
#include "tmtmx.h"
#include "tmlanguage.h"
#include "tmsettings.h"
#include "tmmatchermanager.h"

#include <QFileDialog>
#include <QTextStream>
#include <QMainWindow>
#include <QVBoxLayout>


namespace Ui {
class tmMain;
}

class tmMain : public QMainWindow
{
    Q_OBJECT

public:
    explicit tmMain(QWidget *parent = 0);
    void load();
    void clear();
    bool eventFilter(QObject *o, QEvent *e);
    ~tmMain();

private slots:
    void on_actionExit_triggered();
    void on_actionNew_triggered();
    void on_actionSave_triggered();
    void on_actionOpen_triggered();
    void on_actionSet_corpus_files_triggered();

    void on_actionSet_Dictionary_triggered();

    void on_actionLoad_source_text_triggered();


    void on_actionAbout_triggered();

    void on_actionLoad_Translation_Memory_triggered();

    void on_actionSave_Translation_Memory_triggered();

    void on_pbSave_clicked();

    void on_cbSrcLang_currentIndexChanged(const QString &arg1);

    void on_cbTgtLang_currentIndexChanged(const QString &arg1);

private:
    Ui::tmMain *ui;
    TmProject *prj;
    QVBoxLayout *mBox;
    QString source;
    QList<TmRow *> mRows;
    bool initDone;
};

#endif // TMMAIN_H
