#ifndef TMLABEL_H
#define TMLABEL_H

#include <QWidget>
#include <QPainter>
#include <QEvent>
#include <QMouseEvent>

class TmLabel : public QWidget
{
    Q_OBJECT
public:
    explicit TmLabel(QString text, QWidget *parent = 0);
    inline QString getText(){
        return mText;
    }
protected:
    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent *me);
    bool eventFilter(QObject *object, QEvent *event){
        if(object==this )

        {
            if (event->type()==QEvent::Enter ){
                mTop=true;
                repaint();
                emit mouseEnter();
            } else if ( event->type()==QEvent::Leave){
                mTop=false;
                repaint();
                emit mouseLeave();
            }
        }

        return QObject::eventFilter(object,event);
    }
signals:
    void clicked(QString text);
    void mouseEnter();
    void mouseLeave();
public slots:
private:

    QString mText;
    bool mTop;

};

#endif // TMLABEL_H
