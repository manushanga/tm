#include "tmtmx.h"
#include <QDebug>
#include <QTextDocument>
TmTmx::TmTmx()
{

}

void TmTmx::loadFromFile(QVector< QVector<TmNode> >& data, QString file)
{
    TmxHandler *h=new TmxHandler();
    QXmlSimpleReader reader;
    QFile f(file);
    f.open(QFile::ReadOnly| QFile::Text);
    QXmlInputSource src(&f);
    reader.setContentHandler(h);
    reader.parse(src);
    auto newdata = h->getData();
    foreach (auto a, newdata) {
        data.push_back(a);
    }
    f.close();
}

void TmTmx::saveToFile(QVector<QVector<TmNode> > &data, QString file)
{
    QFile f(file);
    f.open(QFile::WriteOnly| QFile::Text);
    QTextStream ts(&f);
    ts.setCodec("utf-8");
    ts<<("<?xml version=\"1.0\" ?>\n<!DOCTYPE tmx SYSTEM \"tmx13.dtd\">");
    ts<<"<tmx version=\"1.3\">\n";
    ts<<"<header creationtool=\"TM\"\n";
    ts<<"datatype=\"PlainText\"\n";
    ts<<"segtype=\"sentence\">";
    ts<<"</header>\n";
    ts<<"<body>\n";
    foreach (auto a, data) {
        ts<<("<tu>\n");
        foreach(auto b, a) {
            ts<<QString("<tuv xml:lang=\""+toTmx(b.lang)+"\" >\n");
            ts<<QString("<seg>"+Qt::escape(b.text)+"</seg>");
            ts<<QString("</tuv>\n");
        }
        ts<<("</tu>\n");
    }
    ts<<"</body>\n";
    ts<<"</tmx>\n";
    f.close();
}
