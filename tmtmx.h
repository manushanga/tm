#ifndef TMTMX_H
#define TMTMX_H
#include "tmnode.h"

#include <QtXml/QXmlSimpleReader>
#include <QtXml/QXmlDefaultHandler>
#include <QStack>
#include <iostream>
using namespace std;

class TmTmx
{
public:
    TmTmx();
    inline static QString toTmx(QString l){
        return l.toUpper().replace("_","-");
    }
    inline static QString toLocale(QString l){
        auto ls=l.split('-');
        return ls[0].toLower() + "_" + ls[1].toUpper();
    }
    static void loadFromFile(QVector< QVector<TmNode> >& data, QString file);
    static void saveToFile(QVector< QVector<TmNode> >& data, QString file);
};

class TmxHandler : public QXmlDefaultHandler
{
public:

private:
    QVector<TmNode> list;
    QVector< QVector<TmNode> > data;
    QString read;
    bool readSeg;
    int writeTo;
public:
    TmxHandler() : read(""), readSeg(false), writeTo(0) {}
    bool startElement(const QString &namespaceURI, const QString &localName, const QString &qName, const QXmlAttributes &atts) {
        if (localName.toLower() == "tu") {

        } else if (localName.toLower() == "tuv") {
            TmNode tn;
            tn.lang = TmTmx::toLocale(atts.value("xml:lang"));
            list.push_back(tn);

        } else if (localName.toLower() == "seg") {
            readSeg=true;
        }

        return true;
    }
    bool endElement(const QString &namespaceURI, const QString &localName, const QString &qName) {
        if (localName.toLower() == "tu") {
            data.push_back(list);
            list.clear();
            writeTo =0;
        } else if (localName.toLower()=="tuv") {

            writeTo++;
        } else if (localName.toLower() == "seg") {
            readSeg =false;
        }
        return true;
    }
    bool startCDATA(){
        return true;
    }
    bool endCDATA(){
        return true;
    }
    bool characters(const QString &ch){
        if (readSeg) {
            list[writeTo].text = ch;
        }

        return true;
    }
    inline QVector< QVector<TmNode> > &getData(){
        return data;
    }
};



#endif // TMTMX_H
