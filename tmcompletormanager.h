#ifndef TMCOMPLETORMANAGER_H
#define TMCOMPLETORMANAGER_H

#include "tmcompletor.h"
#include <QMutex>

class TmCompletorManager {
private:
    TmCompletor *tmc;
    QMutex mMu;
public:
    static TmCompletorManager *getSingleton()
    {
        static TmCompletorManager tm;
        return &tm;
    }
    void populateList(QList<QString>& list) {
        mMu.lock();
        tmc->populateList(list);
        mMu.unlock();
    }
    TmCompletorManager(){
        tmc=new TmCompletor();
    }
    void join(TmRow *obj) {
        mMu.lock();
        tmc->join(obj);
        mMu.unlock();
    }
    inline int width() {
        return tmc->width();
    }
    void show(){
        mMu.lock();
        tmc->show();
        mMu.unlock();
    }
    void setPosition(int x, int y){
        mMu.lock();
        tmc->setPosition(x,y);
        mMu.unlock();
    }
    void hide(){
        mMu.lock();
        tmc->hide();
        mMu.unlock();
    }
    ~TmCompletorManager(){
    }
};
#endif // TMCOMPLETORMANAGER_H
