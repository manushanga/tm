#include "tmdictionary.h"
#include <QFile>
#include <QTextStream>
#include <QStringList>

TmDictionary::TmDictionary()
{
}

void TmDictionary::addWords(QList<QString> &src, QList<QString> &tgt)
{

    foreach (QString a, src) {
        mList.insert(a, tgt);
    }
}

void TmDictionary::match(QString text, QList< QList<QString>>& list)
{
    auto it=mList.find(text);
    if (it != mList.end()) {
        list.append(it.value());
    }
}

void TmDictionary::loadFromFile(QString filename)
{
    mList.clear();

    if (filename.endsWith("txt")) {
        QFile f(filename);
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        //ts.setCodec("utf-8");
        while (!ts.atEnd()) {
            QString l = ts.readLine();
            QStringList list= l.split(' ',QString::SkipEmptyParts);
            qDebug()<<list;
            if (list.size() == 2) {
                QStringList x,y;
                if (list[0].contains(','))
                    x = list[0].split(',');
                else
                    x = QStringList(list[0]);

                if (list[1].contains(','))
                    y = list[1].split(',');
                else
                    y = QStringList(list[1]);

                auto xx =static_cast< QList<QString> > (x);
                auto yy =static_cast< QList<QString> > (y);
                qDebug()<<xx<<yy;
                addWords(xx, yy);
            }
        }
        f.close();
    }
}
