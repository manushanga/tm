#include "tmcompletor.h"
#include "tmcompletormanager.h"
#include "tmrow.h"
#include "tmmatcher.h"
#include "tmdictionary.h"
#include <iostream>
TmRow::TmRow(QString sourceLine, QString targetLine, QWidget *parent) :
    QWidget(parent),
    mSourceLine(sourceLine),
    mTargetLine(targetLine),
    mState(TmState::Untranslated)

{
    mBox = new QVBoxLayout;
    mTexts = new QHBoxLayout;
    mLower = new QHBoxLayout;

    mLine = new TmEdit;
    mPbState = new QPushButton;

    mPbState->setFixedWidth(fontMetrics().width("Untranslated")*1.5);
    mPbState->setText("Untranslated");

    setLayout(mBox);
    QString part;
    mTexts->addStretch();
    for (int i=0;i<mSourceLine.size();i++) {
        QChar c =  mSourceLine[i];
        if (c==' ') {
            TmLabel *x;

            x =new TmLabel(part);
           // connect(x,SIGNAL(clicked(QString)),this,SLOT(textClicked(QString)));
            connect(x,SIGNAL(mouseEnter()),this,SLOT(textEnter()));
            connect(x,SIGNAL(mouseLeave()),this,SLOT(textLeave()));
            mTexts->addWidget(x);

            part.clear();


        } else if (c=='?'){
            if (part.size()>0) {
                auto x =new TmLabel(part);
                //connect(x,SIGNAL(clicked(QString)),this,SLOT(textClicked(QString)));
                connect(x,SIGNAL(mouseEnter()),this,SLOT(textEnter()));
                connect(x,SIGNAL(mouseLeave()),this,SLOT(textLeave()));
                mTexts->addWidget(x);
                part.clear();
            }
            mTexts->addWidget(new TmLabel("?"));
        } else if (c=='\t' || c=='\r') {
        } else  {
            part+=c;
        }
    }

    if (part.size()>0) {
        auto x =new TmLabel(part);
        //connect(x,SIGNAL(clicked(QString)),this,SLOT(textClicked(QString)));
        connect(x,SIGNAL(mouseEnter()),this,SLOT(textEnter()));
        connect(x,SIGNAL(mouseLeave()),this,SLOT(textLeave()));
        mTexts->addWidget(x);
    }
    mTexts->addStretch();

    setText(mTargetLine);
    mBox->addLayout(mTexts);
    mLower->addWidget(mLine);
    mLower->addWidget(mPbState);

    mBox->addLayout(mLower);
    mLine->setAlignment(Qt::AlignCenter);
    mLine->setFont(QFont("Iskoola Pota"));
    connect(mLine,SIGNAL(clicked()),this,SLOT(targetTextClicked()));
    connect(mLine,SIGNAL(lostFocus()),this,SLOT(targetTextLeave()));
    connect(mPbState,SIGNAL(clicked()),this,SLOT(pbStateClicked()));


}

void TmRow::textEnter()
{
    QList< QList<QString> > ls;
    TmLabel *lb = static_cast<TmLabel*>(sender());

    TmDictionary::getSingleton()->match(lb->getText(), ls);
    if (ls.size() > 0) {
        QPoint p= mapToGlobal(lb->pos());
        TmCompletorManager::getSingleton()->setPosition(p.x(),p.y()+30);
        TmCompletorManager::getSingleton()->join(NULL);

        if (ls.size() == 0) {
            QList<QString> a;
            TmCompletorManager::getSingleton()->populateList(a);
        }
        foreach (auto a, ls){
            TmCompletorManager::getSingleton()->populateList(a);
        }

        TmCompletorManager::getSingleton()->show();
    }
}

void TmRow::textLeave()
{
    TmCompletorManager::getSingleton()->join(NULL);
}

void TmRow::targetTextClicked()
{
    QList<QString> ls;
    TmMatcher::getSingleton()->match(mSourceLine,6, ls);
    if (ls.size() > 0) {
        QPoint p= mapToGlobal(mLine->pos());

        TmCompletorManager::getSingleton()->populateList(ls);
        TmCompletorManager::getSingleton()->setPosition(p.x()
                            -TmCompletorManager::getSingleton()->width()/2
                            +mLine->width()/2,p.y()+30);
        TmCompletorManager::getSingleton()->show();
        TmCompletorManager::getSingleton()->join(this);
    }
}

void TmRow::targetTextLeave()
{
    TmCompletorManager::getSingleton()->join(NULL);
}


void TmRow::pbStateClicked()
{
    TmState state = TmState(((int) mState +1 ) % 2);

    TmCompletorManager::getSingleton()->hide();

    setState(state);
}
