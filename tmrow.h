#ifndef TMROW_H
#define TMROW_H

#include "tmlabel.h"
#include "tmedit.h"
#include "tmmatcher.h"
#include <QPushButton>
#include <QVBoxLayout>

class TmRow : public QWidget
{
    Q_OBJECT

public:
    enum TmState{
        Translated,
        /*Issue,
        Review,*/
        Untranslated
    };
    TmRow(QString sourceLine, QString targetLine="", QWidget *parent=0);
    inline QString getSource(){
        return mSourceLine;
    }
    inline QString getText(){
        return mLine->text();
    }
    inline TmState getState(){
        return mState;
    }
    inline void setText(QString text){
        mLine->setText(text);
        if (!text.trimmed().isEmpty()) {
            setState(Translated);

        }
    }
    inline void setState(TmState state){
        mState = state;
        switch (mState) {
        case Translated:
            mPbState->setText("Translated");
            break;
        /*case Review:
            mPbState->setText("Review");
            break;
        case Issue:
            mPbState->setText("Issue");
            break;*/
        case Untranslated:
            mPbState->setText("Untranslated");
            break;

        }

        if (mState == TmState::Translated) {
            TmMatcher::getSingleton()->addRow(mSourceLine,mLine->text());
        }
    }


public slots:
    //void textClicked(QString text);
    void textEnter();
    void textLeave();
    void targetTextClicked();
    void targetTextLeave();
    void pbStateClicked();
private:

    bool mOntext;
    QHBoxLayout *mTexts;
    QVBoxLayout *mBox;
    QHBoxLayout *mLower;
    QList<TmLabel> mLabels;
    TmEdit *mLine;
    QString mSourceLine;
    QString mTargetLine;
    QPushButton *mPbState;
    TmState mState;
};

#endif // TMROW_H
