#ifndef TMEDIT_H
#define TMEDIT_H

#include <QLineEdit>
#include <QWidget>

class TmEdit : public QLineEdit
{
    Q_OBJECT
public:
    explicit TmEdit(QWidget *parent = 0);
    void mousePressEvent(QMouseEvent *);

signals:
    void clicked();
    void lostFocus();
public slots:

};

#endif // TMEDIT_H
