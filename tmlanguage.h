#include <QStringList>

#ifndef TMLANGUAGE_H
#define TMLANGUAGE_H
class TmLanguage {
public:
    QStringList ql;
    TmLanguage(){
        ql<<"en_US"<<"en_UK"<<"si_LK"<<"ta_LK"<<"ta_IN"<<"fr_FR";
    }
    static TmLanguage *getSingleton(){
        static TmLanguage tl;
        return &tl;
    }
    QStringList &getLanguages(){
        return ql;
    }
};
#endif // TMLANGUAGE_H
