#ifndef TMDICTIONARY_H
#define TMDICTIONARY_H
#include "tmmatcher.h"

#include <QString>

class TmDictionary
{
public:
    TmDictionary();
    static TmDictionary *getSingleton(){
        static TmDictionary tmd;
        return &tmd;
    }
    void addWords(QList<QString>& src, QList<QString>& tgt);
    void match(QString text, QList<QList<QString> > &list);
    void loadFromFile(QString filename);
private:
    QHash<QString, QList<QString> > mList;
};

#endif // TMDICTIONARY_H
