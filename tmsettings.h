#include <QSettings>

#ifndef TMSETTINGS_H
#define TMSETTINGS_H
class TmSettings{
private:
    QSettings qs;
public:
    TmSettings() : qs(QSettings::IniFormat, QSettings::UserScope,"UCSC","tm") {

    }
    QVariant getValue(QString key, QVariant def) {
        QVariant q = qs.value(key,def);
        if (q  == def)
            qs.setValue(key,def);
        return q;
    }
    void setValue(QString key, QVariant val) {
        qs.setValue(key,val);
    }
    QString getSettingsPath(){
        return qs.fileName().section('/',0,-2);
    }
    static TmSettings *getSingleton() {
        static TmSettings ts;
        return &ts;
    }
};
#endif // TMSETTINGS_H
